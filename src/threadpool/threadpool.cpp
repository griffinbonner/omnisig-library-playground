#include <thread>
#include <future>
#include <iostream>
#include <complex>
#include <cstdlib>

#include "../../concurrentqueue/blockingconcurrentqueue.h"

typedef std::complex<float> complex64_t;
typedef std::complex<int16_t> complexi32_t;

/**
 * \brief inference result
 * @param annos - annotations
 * @param num - sequence number of inference call 
*/
struct Result {
    Result(std::string annos, int num) : annos(annos), num(num) {};
    std::string annos;
    int num;
};

/**
 * \brief metadata associated with I/Q data
 * @param num - sequence number of inference call
 * @param sr - sample rate
*/
struct Metadata {
    Metadata(int num, int sr) : num(num), sr(sr) {};
    int num;
    int sr;
};

/**
 * \brief application has member function to perform inference
 * @param sample - radio I/Q samples
 * @param md - minimum metadata required for inference
*/
struct NeuralApp {
    Result inference(complex64_t sample, Metadata md) {
        return Result("anno", md.num);
    }
};

/**
 * \brief multithreaded logging function for debug
 * @param msg - message to write to stdout
 * @param id - thread identifier
*/
void Output(std::string msg, int id) {
    std::stringstream stream;
    stream << "Thread " << id << ": " << msg << std::endl;
    std::cout << stream.str(); 
}

/* ----------------------------------------------------------------------- */

class ThreadPool {
    public:

        explicit ThreadPool(size_t nthreads) {
            workers.reserve(nthreads);
            for (size_t id=0; id != nthreads; ++id) {
                workers.push_back(std::thread(&work, this, id));
            }
        }

        /**
         * \brief flush queue and join worker threads
         * - stop each worker threads work loop
         * - join worker threads
        */
        ~ThreadPool() {
            for (size_t id=0; id != workers.size(); ++id) {
                tasks.enqueue(Task(true));
            }
            for (auto& worker : workers) {
                worker.join();
            }
        }

        /**
         * \brief submit inference task to thread pool
         * @param F - variadic template function to support multiple functions
         * @param args - variadic template args to support multiple parameters
         * @returns - future of type deduced from function return type
        */
        template <typename F, typename... Args>
        auto submit(F&& f, Args&&... args) -> std::future<decltype(f(args...))> {

            /* task to be processed */
            Task task;

            /* create wrapper function from forwarded parameters */
            std::function<decltype(f(args...))()> func = std::bind(std::forward<F>(f), std::forward<Args>(args)...);

            /* package task will automatically update the future when executed asynchronously */
            auto task_ptr = std::make_shared<std::packaged_task<decltype(f(args...))()>>(func);
            
            /* set task wrapper function */
            task.function = [task_ptr](){
                (*task_ptr)();
            };

            /* aquire future set when task is aquired by a worker */
            auto started = task.started();

            /* submit task to task queue */
            tasks.enqueue(std::move(task));

            /* return future after task aquired by worker thread */
            started.wait();
            return std::move(task_ptr->get_future());
        }

        ThreadPool(ThreadPool const&) = delete;
        ThreadPool& operator=(ThreadPool const&) = delete;

        private:

            static void work(ThreadPool* pool, int id) {
                Output("Started...", id);
                Task task;
                moodycamel::ConsumerToken tok(pool->tasks);
                while (true) {
                    pool->tasks.wait_dequeue(tok, task);
                    if (task.stop) {
                        break;
                    }
                    task.aquire();
                    task.function();
                    Output("Called Inference...", id);
                }
            }

        private:
            struct Task {
                explicit Task(bool stop = false) : stop(stop) {}

                Task(Task&& x) noexcept :   function(std::move(x.function)),
                                            aquire(std::move(x.aquire)),
                                            stop(x.stop) {}

                Task& operator=(Task&& x) noexcept {
                    std::swap(stop, x.stop);
                    std::swap(function, x.function);
                    std::swap(aquire, x.aquire);
                    return *this;
                }

                std::future<bool> started() {
                    std::function<bool()> start = [](){return true;};
                    auto ptr = std::make_shared<std::packaged_task<bool()>>(start);
                    aquire = [ptr](){(*ptr)();};
                    return ptr->get_future();
                }

                std::function<void()> function;
                std::function<void()> aquire;
                bool stop;
            };

    private:

        std::vector<std::thread> workers;
        moodycamel::BlockingConcurrentQueue<Task> tasks;
};

int main() {


    int sr = 128; // sample rate
    int nthreads = 6; // number of threads
    int frames = 10; // number of frames to process

    NeuralApp neural_app; // some class that implements method: inference
    auto tp = ThreadPool(nthreads); // thread pool 

    complex64_t samples;
    Metadata meta(1,sr);

    auto function = std::bind(&NeuralApp::inference, neural_app, samples, meta);
    auto fut = tp.submit(function, samples, meta);

    Result res = fut.get();
    std::cout << res.annos << " number: " << res.num << std::endl;

}